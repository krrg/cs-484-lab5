from mpi4py import MPI
from hypercube import *
from sort import HyperSort
from datetime import datetime
from array import array
import sys

comm = MPI.COMM_WORLD

if __name__ == "__main__":

    DIM = int(sys.argv[1])

    S = HyperSort((10**7) / (2**DIM))
    # S = HyperSort(16)

    start = datetime.now()

    for dimension in xrange(DIM):
        S.do_exchange_round(dimension)

    values = sorted(S.values)

    total_time = datetime.now() - start

    print "Time: ", total_time
    print "My partition is of size {}".format(len(values))

    data = array('d', [0])
    comm.Reduce(
        array('d', [float(total_time.total_seconds())]),
        data,
        op=MPI.SUM,
        root=0
    )

    if comm.Get_rank() == 0:
        print 
        print "[[Average time]] = ", data[0] / float(comm.Get_size())


    comm.Barrier()
