import random
from hypercube import *
from mpi4py import MPI
from array import array

comm = MPI.COMM_WORLD

VALUES = 0
HEADER = 1

class HyperSort(object):

    def __init__(self, NUM_ITEMS):
        self.values = array('H', [ HyperSort.gen_value() for x in xrange(NUM_ITEMS) ])
        self.pivots = {}

    @staticmethod
    def gen_value():
        return random.randint(1, 1000)

    def get_pivot(self):
        return self.median()

    def median(self):
        return sorted(self.values)[len(self.values) / 2]

    def gather_median_of_medians(self, dim):
        group_comm = get_pivot_group_comm(dim)

        sendbuf = array('H', [self.median()])
        recvbuf = array('H', xrange(group_comm.Get_size()))

        group_comm.Allgather(
            sendbuf,
            recvbuf
        )

        return sorted(recvbuf)[len(recvbuf) / 2]


    def do_exchange_pivot(self, dim):
        mofm = self.gather_median_of_medians(dim)

        self.pivots[dim] = mofm
        # Send my median to the root node.

        # neighbor = get_neighbor_in_dimension(dim)
        # group_comm = get_pivot_group_comm(dim)
        #
        # if is_pivot_master(dim):
        #     self.pivots[dim] = self.get_pivot()
        #     data = array('H', [self.pivots[dim]])
        #     group_comm.Bcast(data)
        # else:
        #     data = array('H', [0])
        #     group_comm.Bcast(data)
        #     self.pivots[dim] = data[0]

        return self.pivots[dim]


    def do_exchange_round(self, dim):
        greater, lesser = [], []
        neighbor = get_neighbor_in_dimension(dim)

        if len(self.values):
            pivot = self.do_exchange_pivot(dim)
            for v in self.values:
                if v >= pivot:
                    greater.append(v)
                else:
                    lesser.append(v)

        if get_active_in_dimension(dim):
            keep, send = greater, lesser
        else:
            keep, send = lesser, greater


        comm.Isend(array('I', [len(send)]), neighbor, tag=HEADER)
        comm.Isend(array('H', send), neighbor, tag=VALUES)

        recv_buffer = array('I', [0])
        comm.Recv(recv_buffer, source=neighbor, tag=HEADER)
        size = recv_buffer[0]

        recv_buffer = array('H', [0 for i in xrange(size)])
        comm.Recv(recv_buffer, source=neighbor, tag=VALUES)


        self.values = array('H', keep)
        self.values.extend(recv_buffer)
