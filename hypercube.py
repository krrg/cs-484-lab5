from mpi4py import MPI
comm = MPI.COMM_WORLD
DIM = 3

def get_neighbor_in_dimension(dimension):
    bitmask = comm.Get_size() >> (dimension + 1)
    return comm.Get_rank() ^ bitmask

def get_active_in_dimension(dimension):
    bitmask = comm.Get_size() >> (dimension + 1)
    return comm.Get_rank() & bitmask

def get_pivot_master(dimension):
    return comm.Get_rank() - (comm.Get_rank() % (comm.Get_size() >> dimension))

def is_pivot_master(dimension):
    return comm.Get_rank() == get_pivot_master(dimension)

def get_pivot_group(dimension):
    members = range(get_pivot_master(dimension), get_pivot_master(dimension) + (comm.Get_size() >> dimension))
    return comm.Get_group().Dup().Incl(members)

def get_pivot_group_comm(dimension):
    return comm.Create_group(get_pivot_group(dimension))
